<?php
/**
 * Created by PhpStorm.
 * User: ahmed
 * Date: 11/04/2019
 * Time: 18:51
 */

namespace AppBundle\Controller;


use AppBundle\AppBundle;
use FOS\RestBundle\Controller\FOSRestController;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    /**
     * @Route("/users")
     * @Method("GET")
     */
    public function getUsersAction()
    {
        $users = new \stdClass();

        $query = $this->getDoctrine()->getManager();
        $products = $query->getRepository('AppBundle:Users')->findAll();


        foreach ($products as $prod) {
            $users = [ 'matricule' => $prod->getMatricule(),'nom' => $prod->getNom(),'prenom' => $prod->getPrenom(),'password' => $prod->getPassword(),'createdAt' => $prod->getCreatedAt(),'updatedAt' => $prod->getUpdatedAt(),'deletedAt' => $prod->getDeletedAt()];
        }
        return new JsonResponse($users);


    }

    /**
     * @Route("/user")
     * @Method("GET")
     */
    public function getUserAction(Request $request)
    {
        $matricule = $request->query->get('matricule');
        $query = $this->getDoctrine()->getManager();
        $products = $query->getRepository('AppBundle:Users')->findOneBy(['matricule' => $matricule]);
        $user = [ 'matricule' => $products->getMatricule(),'nom' => $products->getNom(),'prenom' => $products->getPrenom(),'password' => $products->getPassword(),'createdAt' => $products->getCreatedAt(),'updatedAt' => $products->getUpdatedAt(),'deletedAt' => $products->getDeletedAt()];
        return new JsonResponse($user);


    }

}